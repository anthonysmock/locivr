﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;
using UnityStandardAssets.Characters.FirstPerson;

public class LaserPointerR : MonoBehaviour {

    public GameObject user;

    private bool locistart = false;

    private Transform _userInitialTransform;
    private FirstPersonController _userFPC;
    private CharacterController _userCC;
    private GameObject[] _lociItems;
    private int _lItemIndex = 0;
    private NavMeshAgent _userAgent;
    private bool _started = false;

    public Transform cameraRigTransform;
    public GameObject teleportReticlePrefab;
    private GameObject reticle;
    private Transform teleportReticleTransform;
    public Transform headTransform;
    public Vector3 teleportReticleOffset;
    public LayerMask teleportMask;
    private bool shouldTeleport;
    
    public GameObject laserPrefab;
    private GameObject laser;
    private Transform laserTransform;
    private Vector3 hitPoint;

    private string cheminfloder = "Sauvegarde\\";

    private List<string> linesSauv;

    private SteamVR_TrackedObject trackedObj;

    private void ShowLaser(RaycastHit hit)
    {
        laser.SetActive(true);
        laserTransform.position = Vector3.Lerp(trackedObj.transform.position, hitPoint, .5f);
        laserTransform.LookAt(hitPoint);
        laserTransform.localScale = new Vector3(laserTransform.localScale.x, laserTransform.localScale.y,
            hit.distance);
    }

    private SteamVR_Controller.Device Controller
    {
        get { return SteamVR_Controller.Input((int)trackedObj.index); }
    }

    void Awake()
    {
        trackedObj = this.GetComponent<SteamVR_TrackedObject>();
    }
    // Use this for initialization
    void Start () {
        laser = Instantiate(laserPrefab);
        laserTransform = laser.transform;
        reticle = Instantiate(teleportReticlePrefab);
        teleportReticleTransform = reticle.transform;
        linesSauv.Add(SceneManager.GetActiveScene().buildIndex.ToString());

        _userAgent = user.GetComponent<NavMeshAgent>();
        _userCC = user.GetComponent<CharacterController>();
        _userFPC = user.GetComponent<FirstPersonController>();
        _userInitialTransform = user.transform;
        _lociItems = GameObject.FindGameObjectsWithTag("LociItem");

        _userAgent.autoBraking = false;
        _userAgent.enabled = false;
    }
	
	// Update is called once per frame
	void Update () {
        
        if (Controller.GetPress(SteamVR_Controller.ButtonMask.Grip))
        {
            RaycastHit hit;
            if (Physics.Raycast(trackedObj.transform.position, transform.forward, out hit, 100))
            {
                hitPoint = hit.point;
                ShowLaser(hit);

                reticle.SetActive(true);
                teleportReticleTransform.position = hitPoint + teleportReticleOffset;
                shouldTeleport = true;
            }
        }
        else 
        {
            laser.SetActive(false);
            reticle.SetActive(false);
        }

        if (Controller.GetHairTriggerDown())
        {
            if (locistart == false)
            {
                Teleport();
            }
            else
            {
                locistart = false;
                stoploci();
            }
            
        }

        if (Controller.GetPress(SteamVR_Controller.ButtonMask.Touchpad))
        {
            if(locistart == false)
            {
                locistart = true;
                startlocipath();
            }else
            {
                //stoploci();
            }
               
        }

        if (Input.GetMouseButtonDown(0))
        {
            savepath2();
        }
            
    }

    private void stoploci()
    {
        GameObject player = GameObject.Find("LociPathManager");
        player.GetComponent<LociPathManager>().StopLociPath();
    }

    private void startlocipath()
    {
        GameObject player = GameObject.Find("LociPathManager");
        player.GetComponent<LociPathManager>().StartLociPath();
    }

    private void Teleport()
    {
        shouldTeleport = false;
        reticle.SetActive(false);
        Vector3 difference = cameraRigTransform.position - headTransform.position;
        difference.y = 0;
        hitPoint.y = 1.8f;
        cameraRigTransform.position = hitPoint + difference;
        linesSauv.Add(cameraRigTransform.position.x + " " + cameraRigTransform.position.y + " " + cameraRigTransform.position.z + " " + cameraRigTransform.rotation.x + " " + cameraRigTransform.rotation.y + " " + cameraRigTransform.rotation.z);
    }

    void savepath2()
    {
        //String pathfichier = cheminfloder + SceneManager.GetActiveScene().name + " - " + DateTime.Now.ToString().Replace('/', '_').Replace(':', '_') + ".txt";

        //System.IO.File.WriteAllLines(@"" + pathfichier + "", linesSauv.ToArray());
        SceneManager.LoadScene(0);
    }
}

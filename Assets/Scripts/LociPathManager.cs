﻿using UnityEngine;
using UnityEngine.AI;
using UnityStandardAssets.Characters.FirstPerson;
using System;

public class LociPathManager : MonoBehaviour
{
    private bool ismenuimage = false;

    private LoadImage image = new LoadImage();

    public GameObject user;

    private Transform _userInitialTransform;
    private FirstPersonController _userFPC;
    private CharacterController _userCC;
    private GameObject[] _lociItems;
    private int _lItemIndex = 0;
    private NavMeshAgent _userAgent;
    private bool _started = false;

    void Start()
    {
        image = new LoadImage();

        _userAgent = user.GetComponent<NavMeshAgent>();
        _userCC = user.GetComponent<CharacterController>();
        _userFPC = user.GetComponent<FirstPersonController>();
        _userInitialTransform = user.transform;
       

        _userAgent.autoBraking = false;
        _userAgent.enabled = false;
        //StartLociPath();

    }

    public LociPathManager(NavMeshAgent userAgent , CharacterController userCC, FirstPersonController userFPC, Transform userInitialTransform, GameObject[] lociItems)
    {
        _userAgent = userAgent;
        _userCC = userCC;
        _userFPC = userFPC;
        _userInitialTransform = userInitialTransform;
        _lociItems = lociItems;
    }



    GameObject[] SortItems(GameObject[] items)
    {
        return items;
    }

    public void StartLociPath()
    {
        updateitems();
        Debug.Log(_lociItems.Length);
        if (_lociItems.Length == 0)
            return;

//        user.transform.SetPositionAndRotation(_userInitialTransform.position, _userInitialTransform.rotation);
//        _userCC.enabled = false;

        _userAgent.enabled = true;

        _lItemIndex = 0;
        _userAgent.destination = _lociItems[_lItemIndex].transform.position;
    }
    private void updateitems()
    {
        _lociItems = SortItems(GameObject.FindGameObjectsWithTag("LociItem"));
    }
    void Update()
    {
        if (_started)
        {
            if (_userAgent.remainingDistance <= 5f)
            {
                if (isLastItem()) StopLociPath();
                else
                {
                    _lItemIndex++;
                    _userAgent.destination = _lociItems[_lItemIndex].transform.position;
                    user.GetComponentInChildren<Camera>().transform.LookAt(_lociItems[_lItemIndex].transform.position);
                }
            }
            else if(_userAgent.remainingDistance <= 20f)
                user.GetComponentInChildren<Camera>().transform.LookAt(_lociItems[_lItemIndex].transform.position);
        }

        if (Input.GetMouseButtonDown(0))
        {
            if (!ismenuimage)
            {
                GameObject player = GameObject.Find("FPSController");
                Vector3 angle_fps = player.transform.rotation.eulerAngles;
                angle_fps = new Vector3(90, angle_fps.y +180 , 0);
                ismenuimage = true;
                image.PutMenu(new Vector3(40, -10, 349), angle_fps);
            }
            else
            {
                GameObject player = GameObject.Find("FPSController");
                Vector3 angle_fps = player.transform.rotation.eulerAngles;
                angle_fps = new Vector3(90, angle_fps.y + 180, 0);
                image.addimage(new Vector3(40, -10, 349), angle_fps);
                //ismenuimage = false;
            }
        }
        if (Input.GetMouseButtonDown(1))
        {
                image.ImageSuiv();
        }
    }

    public void StopLociPath()
    {
        _started = false;
        _userAgent.enabled = false;
    }

    private bool isLastItem()
    {
        return _lItemIndex + 1 == _lociItems.Length;
    }
}
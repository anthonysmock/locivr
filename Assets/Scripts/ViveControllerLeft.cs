﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ViveControllerLeft : MonoBehaviour {

    // 1
    private SteamVR_TrackedObject trackedObj;

    // 2
    private SteamVR_Controller.Device Controller
    {
        get { return SteamVR_Controller.Input((int)trackedObj.index); }
    }

    // 1
    public GameObject laserPrefab;
    // 2
    private GameObject laser;
    // 3
    private Transform laserTransform;
    // 4
    private Vector3 hitPoint;

    private bool ismenuimage = false;

    private LoadImage image = new LoadImage();

    void Awake()
    {
        trackedObj = this.GetComponent<SteamVR_TrackedObject>();
    }

    private void Start()
    {
        // 1
        laser = Instantiate(laserPrefab);
        // 2
        laserTransform = laser.transform;
    }

    private void ShowLaser(RaycastHit hit)
    {
        // 1
        laser.SetActive(true);
        // 2
        laserTransform.position = Vector3.Lerp(trackedObj.transform.position, hitPoint, .5f);
        // 3
        laserTransform.LookAt(hitPoint);
        // 4
        laserTransform.localScale = new Vector3(laserTransform.localScale.x, laserTransform.localScale.y,
            hit.distance);
    }

    // Update is called once per frame
    void Update()
    {
        // 1
        if (Controller.GetPress(SteamVR_Controller.ButtonMask.Grip))
        {
            RaycastHit hit;

            if (Physics.Raycast(trackedObj.transform.position, transform.forward, out hit, 100))
            {
                hitPoint = hit.point;
                ShowLaser(hit);
            }
        }
        else // 3
        {
            laser.SetActive(false);
        }
        if (Controller.GetAxis() != Vector2.zero)
        {
        }

        // 2
        if (Controller.GetHairTriggerDown())
        {
            

            if (!ismenuimage)
            {
                GameObject player = GameObject.Find("FPSController");
                Vector3 angle_fps = player.transform.rotation.eulerAngles;
                angle_fps = new Vector3(90, angle_fps.y + 180, 0);
                ismenuimage = true;
                image.PutMenu(hitPoint, angle_fps);
            }
            else
            {
                GameObject player = GameObject.Find("FPSController");
                Vector3 angle_fps = player.transform.rotation.eulerAngles;
                angle_fps = new Vector3(90, angle_fps.y + 180, 0);
                image.addimage(hitPoint, angle_fps);
                //ismenuimage = false;
            }
        }
        if (Controller.GetPress(SteamVR_Controller.ButtonMask.Touchpad))
        {
            //if(ismenuimage)
                image.ImageSuiv();
        }


        // 3
        if (Controller.GetHairTriggerUp())
        {
            
        }

        // 4
        if (Controller.GetPressDown(SteamVR_Controller.ButtonMask.Grip))
        {
            Debug.Log(gameObject.name + " Grip Press");
        }

        // 5
        if (Controller.GetPressUp(SteamVR_Controller.ButtonMask.Grip))
        {
            Debug.Log(gameObject.name + " Grip Release");
        }
    }

    
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ViveControllerRight : MonoBehaviour {

    // 1
    private SteamVR_TrackedObject trackedObj;

    public GameObject PersoObj;
    public GameObject CameraObj;

    private CharacterController controller;

    public float speed = 7.0f;

    private Vector3 moveDirection = Vector3.zero;

    public void Start()
    {
        controller = GetComponent<CharacterController>();
    }

    // 2
    private SteamVR_Controller.Device Controller
    {
        get { return SteamVR_Controller.Input((int)trackedObj.index); }
    }

    void Awake()
    {
        trackedObj = this.GetComponent<SteamVR_TrackedObject>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Controller.GetAxis() != Vector2.zero)
        {/*
            moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
            moveDirection = transform.TransformDirection(moveDirection);
            moveDirection *= speed;
            controller.Move(moveDirection * Time.deltaTime);

            PersoObj.transform.position = PersoObj.transform.position + new Vector3(Controller.GetAxis().x * Mathf.Cos(CameraObj.transform.localRotation.x), Controller.GetAxis().y * Mathf.Cos(CameraObj.transform.localRotation.z));
        */
        }

        // 2
        if (Controller.GetHairTriggerDown())
        {
            Debug.Log(gameObject.name + " Trigger Press");
        }

        // 3
        if (Controller.GetHairTriggerUp())
        {
            Debug.Log(gameObject.name + " Trigger Release");
        }

        // 4
        if (Controller.GetPressDown(SteamVR_Controller.ButtonMask.Grip))
        {
            Debug.Log(gameObject.name + " Grip Press");
        }

        // 5
        if (Controller.GetPressUp(SteamVR_Controller.ButtonMask.Grip))
        {
            Debug.Log(gameObject.name + " Grip Release");
        }
    }
}

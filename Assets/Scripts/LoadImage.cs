﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

public class LoadImage : MonoBehaviour
{
    public Texture2D texture;
    public GameObject plane;
    public GameObject planeMenu;
    byte[] fileData;
    Vector3 sauvhitPoint;
    Vector3 sauvangle;
    public string path = "C:\\Users\\perre\\Pictures\\";
    public Material material;
    Dictionary<string, int> dictionary = new Dictionary<string, int>();

    // Use this for initialization
    void Start()
    {
        
    }

    public void addimage(Vector3 hitPoint, Vector3 angle )
    {
        //planeMenu.GetComponent<Renderer>().enabled = false;
        plane = GameObject.CreatePrimitive(PrimitiveType.Plane);
        plane.GetComponent<Renderer>().material = material;
        plane.tag = "LociItem";
        plane.transform.localScale *= 0.3f;
        plane.transform.position = new Vector3(hitPoint.x, hitPoint.y +2, hitPoint.z);
        plane.transform.rotation = Quaternion.Euler(angle);
    }

    public void PutMenu(Vector3 hitPoint, Vector3 angle)
    {
        //planeMenu.GetComponent<Renderer>().enabled = true;
        DirectoryInfo d = new DirectoryInfo(@"" + path + "");//Assuming Test is your Folder
        FileInfo[] Files = d.GetFiles("*.png"); //Getting Text files
        Debug.Log(Files.Length);
        int count = 0;
        string filePath = "";
        foreach (FileInfo file in Files)
        {
            if (count == 0) filePath = path + file.Name;
            dictionary.Add(path + file.Name, count);
            count++;
        }
        planeMenu = new GameObject();
        planeMenu = GameObject.CreatePrimitive(PrimitiveType.Plane);
        //plane.AddComponent<BoxCollider>();
        GameObject player = GameObject.Find("FPSController");
        planeMenu.transform.parent = player.transform;
        planeMenu.GetComponent<Renderer>().material = material;
        planeMenu.transform.localScale *= 0.05f;
        planeMenu.transform.localPosition =  new Vector3(-1, 1, 1);
        planeMenu.transform.localRotation =  Quaternion.Euler(new Vector3(90,180,90));

        GameObject lightGameObject = new GameObject();
        Light lightComp = lightGameObject.AddComponent<Light>();
        lightComp.color = Color.white;
        lightGameObject.transform.parent = player.transform;
        lightGameObject.transform.localPosition = new Vector3(-1.5f, 1, 1.5f);

        ChangeTexture(filePath);
    }


    // Update is called once per frame
    void Update()
    {
        
    }

    public void ImageSuiv()
    {
        int value = dictionary[texture.name];
        if (value == dictionary.Count - 1) value = 0;
        else value++;
        string filePath = "";
        filePath = dictionary.Where(kvp => kvp.Value == value).Select(kvp => kvp.Key).FirstOrDefault();
        ChangeTexture(filePath);
    } 

    void ChangeTexture(string filePath)
    {
        fileData = File.ReadAllBytes(filePath);
        texture = new Texture2D(2, 2);
        texture.LoadImage(fileData); //..this will auto-resize the texture dimensions.
        texture.name = filePath;

        material = new Material(Shader.Find("Diffuse"));
        material.mainTexture = texture;
        planeMenu.GetComponent<Renderer>().material = material;
    }

    void OnGUI()
    {
        //GUI.DrawTexture(new Rect(80, -80, 600, 600), texture, ScaleMode.ScaleToFit, true, 10.0F);
        //Debug.Log("Hi");
    }
}

